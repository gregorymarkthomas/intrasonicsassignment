# IS assignment - Java

## Where is the APK?

[android/app/release/gregory_mark_thomas_is_assignment_java.apk](android/app/release/gregory_mark_thomas_is_assignment_java.apk)


## Structure

- Traditional; View and Presenter within an Activity 'MainActivity', and Model via utility classes


## Future development

- Find way to calculate median without sorting array first
	- ```Arrays.sort()``` uses a mergesort, and its time complexity is never better than O(n log n).
	- In the real world the current implementation will be okay, though; nobody is going to input thousands upon thousands of numbers
- Minor visual effects
- UML diagram
- Use "invalid" instead of "NaN" for an invalid value shown in the view
	- Also highlight this in red
- UI testing
	- UI is simple, though, so would do this if more requirements were added
- Model View Presenter
	- Again, only if more requirements were added; right now the requirements are simple, and logic with view code in an Activity (like this version) is fine for now.