package com.gregorymarkthomas.intrasonicsassignment.model;

import androidx.annotation.Nullable;

import java.util.Arrays;

public class MedianArrayCalculator {

    /**
     * Finds the median (the middle) value of an array of numbers.
     *
     * We are finding the "midpoint" element but how we do that depends on if we have an
     * odd or even number of values.
     *
     * What is the midpoint? It is when there is 50% of the elements on the left, and 50% on the right.
     * If there is an ODD number of elements, we can easily pick the midpoint element as there will
     * be an equal number of values either side of it.
     * For EVEN numbers of elements, the midpoint element will not exist in the array;
     * it will probably be a decimal that would be the midpoint between the values of
     * the two "middle" elements.
     * We must use different formula depending on whether there is an odd or even number of input
     * elements.
     * These two formula rely on the array being SORTED.
     *
     * Even: ((arrayLength / 2) + ((arrayLength + 2) / 2)) / 2
     *
     * Odd: (arrayLength + 1) / 2
     *
     * These mathematical formulae return the INDEX of the median that exists in an array of numbers
     * that start from ONE. In the implementation below you will see extra '- 1's being used.
     * The '- 1' (e.g. int upperMidIndex = (int) (medianIndex + 0.5) - 1;) are there because Java
     * starts counting array elements from ZERO and not one; this is an important difference from
     * the mathematical formulae because if we did not minus 1 from the medianIndex, then the
     * incorrect element will be chosen from the array.
     * For the Odd formula, we remove the "+ 1" to achieve the same effect.
     *
     * e.g: 23, 37, 42, 55, 60, 72, 83, 91 (as per assignment sheet)
     * We have 8 elements, so we use the 'even' formula.
     * 8 / 2 = 4
     * (8 + 2) / 2 = 5
     * = (4 + 5) / 2 = 4.5
     * We want the 4.5th element in our array, but of course we only have a 4th and a 5th element.
     * Our 4th element is 55 and our 5th element is 60; what is the midpoint value between those?
     * ((60-55) / 2) + 55 = 57.5
     * 57.5 is our median.
     *
     * Ignores NaN values (by counting them and counteracting them) that appear in the input array.
     * NaN values appear at the END of a sorted array.
     *
     * TODO: 'Arrays.sort(arr)' is doing a mergesort. Remove sorting stage if possible.
     *  Mergesort has a time complexity of O(n log n) (and will never exceed that)
     *
     * @param arr: the array of doubles to find median of.
     * @return median of array {@param arr}, or null if {@param arr} is empty.
     */
    @Nullable
    public static Double getMedian(double[] arr) {
        Double median = null;
        if(arr.length > 0) {
            Arrays.sort(arr);
            int nanCount = getNaNCount(arr);
            int arrLengthNoNaNs = arr.length - nanCount;
            if(arrLengthNoNaNs > 0) {
                if (arrLengthNoNaNs % 2 == 0) {
                    double medianIndex = (((arrLengthNoNaNs / 2.0) + ((arrLengthNoNaNs + 2) / 2.0)) / 2.0);
                    int upperMidIndex = (int) (medianIndex + 0.5) - 1;
                    int lowerMidIndex = (int) (medianIndex - 0.5) - 1;
                    median = ((arr[upperMidIndex] - arr[lowerMidIndex]) / 2) + arr[lowerMidIndex];
                } else {
                    int medianIndex = arrLengthNoNaNs / 2;
                    median = arr[medianIndex];
                }
            }
        }
        return median;
    }


    /**
     * Gets count of '"Not A Number"s' (NaNs) that are found within {@param arr}.
     * @param arr
     * @return count of NaNs
     */
    private static int getNaNCount(final double[] arr) {
        int count = 0;
        for(double num: arr) {
            if(Double.isNaN(num))
                count++;
        }
        return count;
    }
}
