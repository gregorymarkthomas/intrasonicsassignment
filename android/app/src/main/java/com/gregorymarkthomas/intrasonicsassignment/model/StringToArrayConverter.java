package com.gregorymarkthomas.intrasonicsassignment.model;

public class StringToArrayConverter {

    private static final String TAG = "StringToArrayConverter";

    private static final String ALLOWED_STRING_SEPARATORS = "[,\\s]{1,}|[,]{1,}|[\\s]{1,}|[.]{2,}";

    /**
     * Takes in a string of numbers and commas and/or spaces and/or line breaks to separate them.
     * Includes "Not A Numbers" in the output for any garbage input.
     *
     * @param arrStr e.g. 23 37 42 55 60 72 83 91
     * @return a double array containing valid doubles for valid strings and NaN doubles for invalid strings
     */
    public static double[] convert(String arrStr) {
        String[] splitStrings = arrStr.split(ALLOWED_STRING_SEPARATORS);
        double[] doubleArr = new double[splitStrings.length];
        for(int i = 0; i < splitStrings.length; i++) {
            doubleArr[i] = parseDouble(splitStrings[i]);
        }
        return doubleArr;
    }

    /**
     * Converts a string into a double.
     * This returns a Not A Number (NaN) double if String is invalid.
     * @param str a 'numeric' string, like "3.2"
     * @return double if converted, NaN if not.
     */
    private static double parseDouble(String str) {
        double parsed;
        try {
            parsed = Double.parseDouble(str);
        } catch(NumberFormatException e) {
            parsed = Double.NaN;
        }
        return parsed;
    }
}
