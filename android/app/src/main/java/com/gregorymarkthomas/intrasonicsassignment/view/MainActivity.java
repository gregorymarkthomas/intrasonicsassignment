package com.gregorymarkthomas.intrasonicsassignment.view;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.gregorymarkthomas.intrasonicsassignment.R;
import com.gregorymarkthomas.intrasonicsassignment.model.MeanArrayCalculator;
import com.gregorymarkthomas.intrasonicsassignment.model.MedianArrayCalculator;
import com.gregorymarkthomas.intrasonicsassignment.model.StringToArrayConverter;

import java.util.Locale;

/**
 * MainActivity will be the only view.
 * The assignment does not warrant use of Fragments, or a Fragment-less Model View Presenter
 * implementation.
 * This means that View's controller code resides in MainActivity.
 *
 * It extends AppCompatActivity so that older Android versions can use Android Support Library
 * (if needed).
 */
public class MainActivity extends AppCompatActivity {

    private final String TAG = "MainActivity";

    private final String INPUT_STRING_STATE_KEY = "input_key_state_key";
    private final String MEAN_STRING_STATE_KEY = "mean_key_state_key";
    private final String MEDIAN_STRING_STATE_KEY = "median_key_state_key";

    private EditText inputEditText;
    private Button calculateButton;
    private Button clearButton;
    private TextView meanTextView;
    private TextView medianTextView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.main_activity, null);
        setContentView(view);

        String inputStr = "";
        String meanStr = "";
        String medianStr = "";
        boolean isButtonEnabled = false;
        if(state != null) {
            inputStr = state.getString(INPUT_STRING_STATE_KEY);
            meanStr = state.getString(MEAN_STRING_STATE_KEY);
            medianStr = state.getString(MEDIAN_STRING_STATE_KEY);
            isButtonEnabled = inputStr.length() > 0;
        }

        bindInputEditText(view);
        updateEditText(inputEditText, inputStr);
        inputEditText.requestFocus();

        bindCalculateButton(view);
        setButtonEnabled(calculateButton, isButtonEnabled);

        bindClearButton(view);
        setButtonEnabled(clearButton, isButtonEnabled);

        bindMeanTextView(view);
        updateTextView(meanTextView, meanStr);

        bindMedianTextView(view);
        updateTextView(medianTextView, medianStr);
    }

    @Override
    public final void onSaveInstanceState(@NonNull Bundle state) {
        super.onSaveInstanceState(state);
        state.putString(INPUT_STRING_STATE_KEY, inputEditText.getText().toString());
        state.putString(MEAN_STRING_STATE_KEY, meanTextView.getText().toString());
        state.putString(MEDIAN_STRING_STATE_KEY, medianTextView.getText().toString());
    }

    /**
     * Sets up behaviour of the main EditText.
     * When user presses ENTER on the soft keyboard, the calculation on the array should occur.
     * When a user enters into the EditText, the calculate and clear buttons will enable.
     * @param layoutView
     */
    private void bindInputEditText(View layoutView) {
        inputEditText = layoutView.findViewById(R.id.input_edit_text);
        inputEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    calculateInput();
                    handled = true;
                }
                return handled;

            }
        });
        inputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Do nothing
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean enabled = s.length() > 0;
                setButtonEnabled(calculateButton, enabled);
                setButtonEnabled(clearButton, enabled);
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Do nothing
            }
        });
    }

    private void bindCalculateButton(View layoutView) {
        calculateButton = layoutView.findViewById(R.id.calculate_button);
        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateInput();
            }
        });
    }

    private void bindClearButton(View layoutView) {
        clearButton = layoutView.findViewById(R.id.clear_button);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
            }
        });
    }

    private void bindMeanTextView(View layoutView) {
        meanTextView = layoutView.findViewById(R.id.mean_text_view);
    }

    private void bindMedianTextView(View layoutView) {
        medianTextView = layoutView.findViewById(R.id.median_text_view);
    }

    private void updateEditText(final EditText view, final String str) {
        if(view != null) {
            view.setText(str);
        } else {
            Log.e(TAG, "updateEditText(): view not set");
        }
    }

    private void updateTextView(final TextView view, final String str) {
        if(view != null) {
            view.setText(str);
        } else {
            Log.e(TAG, "updateTextView(): view not set");
        }
    }

    private void setButtonEnabled(final Button button, final boolean enabled) {
        if(button != null) {
            button.setEnabled(enabled);
        } else {
            Log.e(TAG, "setButtonEnabled(): button not set");
        }
    }

    /**
     * Calls backend to receive the statistical elements of our input array and sets it to the view.
     */
    private void calculateInput() {
        String arrStr = inputEditText.getText().toString();
        updateTextView(meanTextView, createStatisticalElementString(R.string.mean_equals,
                MeanArrayCalculator.getMean(StringToArrayConverter.convert(arrStr))));

        updateTextView(medianTextView, createStatisticalElementString(R.string.median_equals,
                MedianArrayCalculator.getMedian(StringToArrayConverter.convert(arrStr))));
    }

    private void clear() {
        updateEditText(inputEditText, "");
        inputEditText.requestFocus();
        updateTextView(meanTextView, "");
        updateTextView(medianTextView, "");
    }

    private String createStatisticalElementString(final int titleStrResource,
                                                  final Double elementValue) {
        String output = getString(titleStrResource);
        if(elementValue != null) {
            output += String.format(Locale.getDefault(), "%.1f", elementValue);
        } else {
            output += getString(R.string.invalid);
        }
        return output;
    }
}
