package com.gregorymarkthomas.intrasonicsassignment.model;

import androidx.annotation.Nullable;

public class MeanArrayCalculator {
    /**
     * Finds the mean (average) value of an array of numbers.
     *
     * Adds all the elements up and divides by how many elements there are.
     *
     * Ignores NaN values that appear in the input array.
     *
     * @param arr: the array of doubles to find mean of.
     * @return mean of array {@param arr}, or null if {@param arr} is empty.
     */
    @Nullable
    public static Double getMean(double[] arr) {
        Double mean = null;
        if(arr.length > 0) {
            double total = 0.0;
            int nanCount = 0;
            for (double val: arr) {
                if(!Double.isNaN(val))
                    total += val;
                else
                    nanCount++;
            }
            if(arr.length - nanCount > 0)
                mean = total/(arr.length - nanCount);
        }
        return mean;
    }
}
