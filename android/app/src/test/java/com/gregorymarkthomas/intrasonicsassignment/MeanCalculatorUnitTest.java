package com.gregorymarkthomas.intrasonicsassignment;

import org.junit.Test;

import static com.gregorymarkthomas.intrasonicsassignment.model.MeanArrayCalculator.getMean;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;

/**
 * Tests getMean() in NumberArrayCalculator.
 *
 * The 'delta' input of assertEquals is required with value of 0 to stop
 * "Ambiguous method call. Both assertEquals(Object, Object) in Assert and assertEquals
 * (double, double) in Assert match" error.
 * This error occurs because getMean() returns a Double and not double; the compiler does not know
 * which assertEquals function (out of (double, double) and (Object, Object)) I want it to use.
 * Setting the delta tells the compiler I am expecting a legitimate double back. The 0 used for
 * delta says that we are expecting our result double to be exactly what we say it will be, and there
 * will be no variance (say, 0.0001).
 *
 * If the size of the input array for some tests is too large (think 1000000000),
 * then heap space for JVM may be an issue.
 * A value of 1000000000 causes "java.lang.OutOfMemoryError: Java heap space" on Gregory Thomas's
 * Android Studio install (Linux Mint 19).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class MeanCalculatorUnitTest {

    @Test
    public void empty_array_returns_null() {
        assertNull(getMean(new double[] {}));
    }

    @Test
    public void populated_array_returns_Double() {
        assertTrue(getMean(new double[] {5}) instanceof Double);
    }

    @Test
    public void single_input_returns_same() {
        assertEquals(5, getMean(new double[] {5}), 0);
    }

    @Test
    public void single_negative_input_returns_same() {
        assertEquals(-5, getMean(new double[] {-5}), 0);
    }

    /**
     * The assignment says that the mean of this array is 57.9.
     * In this app, the mean value is NOT rounded upwards unless a view's controller code does it
     * itself.
     */
    @Test
    public void assignment_example() {
        assertEquals(57.875, getMean(new double[] {23, 37, 42, 55, 60, 72, 83, 91}), 0);
    }

    @Test
    public void negative_whole_numbers_return_value() {
        assertEquals(-2.0, getMean(new double[] {-2, -2}), 0);
    }

    @Test
    public void decimal_numbers_return_decimal_value() {
        assertEquals(2, getMean(new double[] {2.4, 1.6}), 0);
    }

    @Test
    public void negative_decimal_numbers_return_decimal_value() {
        assertEquals(-2, getMean(new double[] {-2.4, -1.6}), 0);
    }

    /**
     * The rules for calculating averages of positive and negative numbers are (should be) the same.
     */
    @Test
    public void positive_and_negative_whole_numbers() {
        assertEquals(-2, getMean(new double[] {-16, -28, 14, -10, 30}), 0);
    }

    /**
     * Test to check how function fares (in time and memory) with arrLength number of inputs.
     */
    @Test
    public void large_random_input() {
        int arrLength = 100000000;
        double[] arr = new double[arrLength];

        double total = 0;
        for(int i = 0; i < arrLength; i++) {
            arr[i] = Math.random();
            total += arr[i];
        }
        double mean = total / arrLength;

        assertEquals(mean, getMean(arr), 0);
    }

    @Test
    public void zero_input_returns_zero() {
        assertEquals(0, getMean(new double[]{0}), 0);
    }

    @Test
    public void ignores_nans_in_array() {
        assertEquals(57.875, getMean(new double[] {Double.NaN, 23, 37, 42, 55, Double.NaN, 60, 72, 83, Double.NaN, 91, Double.NaN}), 0);
    }

    @Test
    public void nan_returns_null() {
        assertNull(getMean(new double[] {Double.NaN}));
    }
}