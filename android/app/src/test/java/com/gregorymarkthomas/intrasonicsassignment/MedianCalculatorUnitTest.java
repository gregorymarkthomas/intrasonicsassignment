package com.gregorymarkthomas.intrasonicsassignment;

import org.junit.Test;

import static com.gregorymarkthomas.intrasonicsassignment.model.MedianArrayCalculator.getMedian;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;


/**
 * Tests getMedian() in NumberArrayCalculator.
 *
 * The 'delta' input of assertEquals is required with value of 0 to stop
 * "Ambiguous method call. Both assertEquals(Object, Object) in Assert and assertEquals
 * (double, double) in Assert match" error.
 * This error occurs because getMedian() returns a Double and not double; the compiler does not know
 * which assertEquals function (out of (double, double) and (Object, Object)) I want it to use.
 * Setting the delta tells the compiler I am expecting a legitimate double back. The 0 used for
 * delta says that we are expecting our result double to be exactly what we say it will be, and there
 * will be no variance (say, 0.0001).
 *
 * If the size of the input array for some tests is too large (think 1000000000),
 * then heap space for JVM may be an issue.
 * A value of 1000000000 causes "java.lang.OutOfMemoryError: Java heap space" on Gregory Thomas's
 * Android Studio install (Linux Mint 19).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class MedianCalculatorUnitTest {
    @Test
    public void empty_array_returns_null() {
        assertNull(getMedian(new double[] {}));
    }

    @Test
    public void populated_array_returns_Double() {
        assertTrue(getMedian(new double[] {5}) instanceof Double);
    }

    @Test
    public void single_input_returns_same() {
        assertEquals(5, getMedian(new double[] {5}), 0);
    }

    @Test
    public void assignment_example() {
        assertEquals(57.5, getMedian(new double[] {23, 37, 42, 55, 60, 72, 83, 91}), 0);
    }

    @Test
    public void single_negative_input_returns_same() {
        assertEquals(-5, getMedian(new double[] {-5}), 0);
    }

    @Test
    public void decimal_numbers_return_decimal_value() {
        assertEquals(2.1, getMedian(new double[] {2.4, 1.8}), 0);
    }

    @Test
    public void negative_decimal_numbers_return_decimal_value() {
        assertEquals(-2.1, getMedian(new double[] {-2.4, -1.8}), 0);
    }

    /**
     * The rules for calculating median of positive and negative numbers are (should be) the same.
     * We expect the function to order the array as -28, -16, -10, 14, 30.
     */
    @Test
    public void positive_and_negative_whole_numbers() {
        assertEquals(-10, getMedian(new double[] {-16, -28, 14, -10, 30}), 0);
    }

    /**
     * Test to check how function fares (in time and memory) with arrLength number of inputs.
     * I am expecting this to be slow at both the for loop in this test (a bit slow),
     * AND within NumberArrayCalculator.getMedian(); it currently sorts the array before finding
     * the median. Arrays.sort() is currently used, which is a mergesort (O(n log n)).
     *
     * TODO: optimise NumberArrayCalculator.getMedian()
     *
     * 'median' variable relies on arrLength being an ODD number.
     */
    @Test
    public void large_input() {
        int arrLength = 99999999;
        double[] arr = new double[arrLength];

        for(int i = arrLength-1; i >= 0; i--) {
            arr[i] = i;
        }
        double median = arr[arrLength/2];
        assertEquals(median, getMedian(arr), 0);
    }

    @Test
    public void ignores_nans_in_array() {
        assertEquals(57.5, getMedian(new double[] {Double.NaN, 23, 37, 42, 55, Double.NaN, 60, 72, 83, Double.NaN, 91, Double.NaN}), 0);
    }

    @Test
    public void nan_returns_null() {
        assertNull(getMedian(new double[] {Double.NaN}));
    }

    @Test
    public void one_digit_one_nan() {
        assertEquals(7.0, getMedian(new double[] {7.0, Double.NaN}), 0.0);
    }
}