package com.gregorymarkthomas.intrasonicsassignment;

import com.gregorymarkthomas.intrasonicsassignment.model.StringToArrayConverter;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;


/**
 * Tests the regex of the convert() function within StringToArrayConverter.
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class StringToArrayConverterUnitTest {
    @Test
    public void assignment_example() {
        double[] arr = StringToArrayConverter.convert("23,37,42,55,60,72,83,91");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void separate_by_space() {
        double[] arr = StringToArrayConverter.convert("23 37 42 55 60 72 83 91");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void separate_by_space_or_comma() {
        double[] arr = StringToArrayConverter.convert("23,37 42,55 60,72 83,91");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void separate_by_space_or_comma_or_linebreak() {
        double[] arr = StringToArrayConverter.convert("23, 37 42, 55\n60, 72 83, 91");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void separate_by_multiple_spaces() {
        double[] arr = StringToArrayConverter.convert("23  37   42    55      60       72        83         91         ");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void separate_by_multiple_commas() {
        double[] arr = StringToArrayConverter.convert("23,37,,42,,,55,,,,60,,,,,72,,,,,,83,,,,,,,91");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void comma_with_space_is_ignored() {
        double[] arr = StringToArrayConverter.convert("23, 37, 42, 55, , 60, 72, 83, 91");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void separate_by_comma_and_space() {
        double[] arr = StringToArrayConverter.convert("23, 37, 42, 55, 60, 72, 83, 91");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void separate_by_line_break() {
        double[] arr = StringToArrayConverter.convert("23\n37\n42\n55\n60\n72\n83\n91\n");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void separate_by_comma_and_space_and_linebreak() {
        double[] arr = StringToArrayConverter.convert("23, \n37, \n42, \n55, \n60, \n72, \n83, \n91");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void separate_by_comma_and_linebreak_and_space() {
        double[] arr = StringToArrayConverter.convert("23,\n 37,\n 42,\n 55,\n 60,\n 72,\n 83,\n 91");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void separate_by_space_and_comma_and_linebreak() {
        double[] arr = StringToArrayConverter.convert("23 ,\n37 ,\n42 ,\n55 ,\n60 ,\n72 ,\n83 ,\n91");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void separate_by_space_and_linebreak_and_comma_() {
        double[] arr = StringToArrayConverter.convert("23 \n,37 \n,42 \n,55 \n,60 \n,72 \n,83 \n,91");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void separate_by_linebreak_and_space_and_comma() {
        double[] arr = StringToArrayConverter.convert("23\n ,37\n ,42\n ,55\n ,60\n ,72\n ,83\n ,91");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void separate_by_linebreak_and_comma_and_space() {
        double[] arr = StringToArrayConverter.convert("23\n, 37\n, 42\n, 55\n, 60\n, 72\n, 83\n, 91");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void comma_at_start() {
        double[] arr = StringToArrayConverter.convert(",23,37,42,55,60,72,83,91");
        assertEquals(9, arr.length);
        assertEquals(Double.NaN, arr[0], 0);
        assertEquals(23.0, arr[1], 0);
        assertEquals(37.0, arr[2], 0);
        assertEquals(42.0, arr[3], 0);
        assertEquals(55.0, arr[4], 0);
        assertEquals(60.0, arr[5], 0);
        assertEquals(72.0, arr[6], 0);
        assertEquals(83.0, arr[7], 0);
        assertEquals(91.0, arr[8], 0);
    }

    @Test
    public void double_comma_at_start() {
        double[] arr = StringToArrayConverter.convert(",,23,37,42,55,60,72,83,91");
        assertEquals(9, arr.length);
        assertEquals(Double.NaN, arr[0], 0);
        assertEquals(23.0, arr[1], 0);
        assertEquals(37.0, arr[2], 0);
        assertEquals(42.0, arr[3], 0);
        assertEquals(55.0, arr[4], 0);
        assertEquals(60.0, arr[5], 0);
        assertEquals(72.0, arr[6], 0);
        assertEquals(83.0, arr[7], 0);
        assertEquals(91.0, arr[8], 0);
    }

    @Test
    public void whitespace_at_start() {
        double[] arr = StringToArrayConverter.convert(" 23 37 42 55 60 72 83 91");
        assertEquals(9, arr.length);
        assertEquals(Double.NaN, arr[0], 0);
        assertEquals(23.0, arr[1], 0);
        assertEquals(37.0, arr[2], 0);
        assertEquals(42.0, arr[3], 0);
        assertEquals(55.0, arr[4], 0);
        assertEquals(60.0, arr[5], 0);
        assertEquals(72.0, arr[6], 0);
        assertEquals(83.0, arr[7], 0);
        assertEquals(91.0, arr[8], 0);
    }

    @Test
    public void double_whitespace_at_start() {
        double[] arr = StringToArrayConverter.convert("  23 37 42 55 60 72 83 91");
        assertEquals(9, arr.length);
        assertEquals(Double.NaN, arr[0], 0);
        assertEquals(23.0, arr[1], 0);
        assertEquals(37.0, arr[2], 0);
        assertEquals(42.0, arr[3], 0);
        assertEquals(55.0, arr[4], 0);
        assertEquals(60.0, arr[5], 0);
        assertEquals(72.0, arr[6], 0);
        assertEquals(83.0, arr[7], 0);
        assertEquals(91.0, arr[8], 0);
    }

    @Test
    public void comma_at_end() {
        double[] arr = StringToArrayConverter.convert("23,37,42,55,60,72,83,91,");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void whitespace_at_end() {
        double[] arr = StringToArrayConverter.convert("23 37 42 55 60 72 83 91 ");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void comma_at_start_then_minus_value() {
        double[] arr = StringToArrayConverter.convert(",-23,37,42,55,60,72,83,91");
        assertEquals(9, arr.length);
        assertEquals(Double.NaN, arr[0], 0);
        assertEquals(-23.0, arr[1], 0);
        assertEquals(37.0, arr[2], 0);
        assertEquals(42.0, arr[3], 0);
        assertEquals(55.0, arr[4], 0);
        assertEquals(60.0, arr[5], 0);
        assertEquals(72.0, arr[6], 0);
        assertEquals(83.0, arr[7], 0);
        assertEquals(91.0, arr[8], 0);
    }

    @Test
    public void minus_with_whitespace() {
        double[] arr = StringToArrayConverter.convert("-23 -37 -42 -55 -60 -72 -83 -91");
        assertEquals(8, arr.length);
        assertEquals(-23.0, arr[0], 0);
        assertEquals(-37.0, arr[1], 0);
        assertEquals(-42.0, arr[2], 0);
        assertEquals(-55.0, arr[3], 0);
        assertEquals(-60.0, arr[4], 0);
        assertEquals(-72.0, arr[5], 0);
        assertEquals(-83.0, arr[6], 0);
        assertEquals(-91.0, arr[7], 0);
    }

    @Test
    public void minus_with_linebreak() {
        double[] arr = StringToArrayConverter.convert("-23\n-37\n-42\n-55\n-60\n-72\n-83\n-91");
        assertEquals(8, arr.length);
        assertEquals(-23.0, arr[0], 0);
        assertEquals(-37.0, arr[1], 0);
        assertEquals(-42.0, arr[2], 0);
        assertEquals(-55.0, arr[3], 0);
        assertEquals(-60.0, arr[4], 0);
        assertEquals(-72.0, arr[5], 0);
        assertEquals(-83.0, arr[6], 0);
        assertEquals(-91.0, arr[7], 0);
    }

    @Test
    public void minus_at_start() {
        double[] arr = StringToArrayConverter.convert("-,-23,-37,-42,-55,-60,-72,-83,-91");
        assertEquals(9, arr.length);
        assertEquals(Double.NaN, arr[0], 0);
        assertEquals(-23.0, arr[1], 0);
        assertEquals(-37.0, arr[2], 0);
        assertEquals(-42.0, arr[3], 0);
        assertEquals(-55.0, arr[4], 0);
        assertEquals(-60.0, arr[5], 0);
        assertEquals(-72.0, arr[6], 0);
        assertEquals(-83.0, arr[7], 0);
        assertEquals(-91.0, arr[8], 0);
    }

    @Test
    public void minus_and_whitespace_and_comma() {
        double[] arr = StringToArrayConverter.convert("-23 ,-37 ,-42 ,-55 ,-60 ,-72 ,-83 ,-91");
        assertEquals(8, arr.length);
        assertEquals(-23.0, arr[0], 0);
        assertEquals(-37.0, arr[1], 0);
        assertEquals(-42.0, arr[2], 0);
        assertEquals(-55.0, arr[3], 0);
        assertEquals(-60.0, arr[4], 0);
        assertEquals(-72.0, arr[5], 0);
        assertEquals(-83.0, arr[6], 0);
        assertEquals(-91.0, arr[7], 0);
    }

    @Test
    public void minus_and_comma_and_whitespace() {
        double[] arr = StringToArrayConverter.convert("-23, -37, -42, -55, -60, -72, -83, -91");
        assertEquals(8, arr.length);
        assertEquals(-23.0, arr[0], 0);
        assertEquals(-37.0, arr[1], 0);
        assertEquals(-42.0, arr[2], 0);
        assertEquals(-55.0, arr[3], 0);
        assertEquals(-60.0, arr[4], 0);
        assertEquals(-72.0, arr[5], 0);
        assertEquals(-83.0, arr[6], 0);
        assertEquals(-91.0, arr[7], 0);
    }

    @Test
    public void dots_at_end() {
        double[] arr = StringToArrayConverter.convert("23,37,42,55,60,72,83,91...........");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void just_minus() {
        double[] arr = StringToArrayConverter.convert("-");
        assertEquals(1, arr.length);
        assertEquals(Double.NaN, arr[0], 0);
    }

    @Test
    public void just_comma() {
        double[] arr = StringToArrayConverter.convert(",");
        assertEquals(0, arr.length);
    }

    @Test
    public void just_space() {
        double[] arr = StringToArrayConverter.convert(" ");
        assertEquals(0, arr.length);
    }

    @Test
    public void decimal_strings() {
        double[] arr = StringToArrayConverter.convert("23.0,37.0,42.0,55.0,60.0,72.0,83.0,91.0");
        assertEquals(8, arr.length);
        assertEquals(23.0, arr[0], 0);
        assertEquals(37.0, arr[1], 0);
        assertEquals(42.0, arr[2], 0);
        assertEquals(55.0, arr[3], 0);
        assertEquals(60.0, arr[4], 0);
        assertEquals(72.0, arr[5], 0);
        assertEquals(83.0, arr[6], 0);
        assertEquals(91.0, arr[7], 0);
    }

    @Test
    public void single_digit_and_multiple_spaces() {
        double[] arr = StringToArrayConverter.convert("7              ");
        assertEquals(1, arr.length);
        assertEquals(7, arr[0], 0);
    }
}