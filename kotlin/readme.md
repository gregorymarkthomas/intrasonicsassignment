# IS assignment - Kotlin

## Where is the APK?

[kotlin/app/release/gregory_mark_thomas_is_assignment_kotlin.apk](kotlin/app/release/gregory_mark_thomas_is_assignment_kotlin.apk)


## Features

- Model View Presenter structure
	- View in the form of an Android Activity
- Presenter and Model testing
- Same behaviour as [Java version](../android/)


## Future development

- Find way to calculate median without sorting array first
	- ```Arrays.sort()``` uses a mergesort, and its time complexity is never better than O(n log n).
	- In the real world the current implementation will be okay, though; nobody is going to input thousands upon thousands of numbers
- Branding
- Minor visual effects
- UML diagram