package com.gregorymarkthomas.intrasonicsassignmentkotlin

import com.gregorymarkthomas.intrasonicsassignmentkotlin.model.ModelContract
import com.gregorymarkthomas.intrasonicsassignmentkotlin.presenter.CalculationViewState
import com.gregorymarkthomas.intrasonicsassignmentkotlin.presenter.Presenter
import com.gregorymarkthomas.intrasonicsassignmentkotlin.view.MainActivityContract
import org.junit.jupiter.api.BeforeAll
import io.mockk.clearMocks
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PresenterTest {
    private val view: MainActivityContract = mockk(relaxUnitFun = true)
    private val model: ModelContract = mockk(relaxUnitFun = true)

    @BeforeAll
    fun init() {
        clearMocks(view, model)
    }

    @Test
    fun `empty input box on init when no saved state`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        // then
        verify { view.setInputBoxText("") }
    }

    @Test
    fun `input box text reapplied from saved state on init`() {
        // when
        val presenter = Presenter(view, CalculationViewState("1 , 2, 4 , 5"), model)

        // then
        verify { view.setInputBoxText("1 , 2, 4 , 5") }
    }

    @Test
    fun `calculate button disabled when empty input box on init`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        // then
        verify { view.disableCalculateButton() }
    }

    @Test
    fun `calculate button enabled when input box filled on init`() {
        // when
        val presenter = Presenter(view, CalculationViewState("1"), model)

        // then
        verify { view.enableCalculateButton() }
    }

    @Test
    fun `clear button disabled on init with empty input box`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        // then
        verify { view.disableClearButton() }
    }

    @Test
    fun `clear button enabled when input box filled on init`() {
        // when
        val presenter = Presenter(view, CalculationViewState("1"), model)

        // then
        verify { view.enableClearButton() }
    }

    @Test
    fun `empty mean label on init when no state`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        // then
        verify { view.setMeanLabel("") }
    }

    @Test
    fun `filled mean label on init with state`() {
        // when
        val presenter = Presenter(view, CalculationViewState("", "1"), model)

        // then
        verify { view.setMeanLabel("1") }
    }

    @Test
    fun `empty median label on init when no state`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        // then
        verify { view.setMedianLabel("") }
    }

    @Test
    fun `filled median label on init with state`() {
        // when
        val presenter = Presenter(view, CalculationViewState("", "", "1"), model)

        // then
        verify { view.setMedianLabel("1") }
    }

    @Test
    fun `set calculate button onclick listener on init`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        // then
        verify { view.setCalculateButtonListener() }
    }

    @Test
    fun `set input box keyboard enter button press listener on init`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        // then
        verify { view.setInputBoxKeyboardEnterButtonBehaviour() }
    }

    @Test
    fun `set input box on text changed listener on init`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        // then
        verify { view.setInputBoxTextChangedListener() }
    }

    @Test
    fun `set clear button onlclick listener on init`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        // then
        verify { view.setClearButtonListener() }
    }


    @Test
    fun `set mean on keyboard enter press`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), TestModel(1.0, 99.0))

        // input ignored; uses TestModel inputs
        presenter.onKeyboardEnterButtonPress("1,2")

        // then
        verify { view.setMeanLabel(TestModel.meanPreText + 1.0) }
    }

    @Test
    fun `set invalid mean on keyboard enter press`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), TestModel(Double.NaN, Double.NaN))

        // input ignored; uses TestModel inputs
        presenter.onKeyboardEnterButtonPress("")

        // then
        verify { view.setMeanLabel("") }
    }

    @Test
    fun `set median on keyboard enter press`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), TestModel(1.0, 99.0))

        // input ignored; uses TestModel inputs
        presenter.onKeyboardEnterButtonPress("1")

        // then
        verify { view.setMedianLabel(TestModel.medianPreText + 99.0) }
    }

    @Test
    fun `set invalid median on keyboard enter press`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), TestModel(Double.NaN, Double.NaN))

        // input ignored; uses TestModel inputs
        presenter.onKeyboardEnterButtonPress("")

        // then
        verify { view.setMedianLabel(TestModel.medianPreText + TestModel.invalid) }
    }



    @Test
    fun `enable calculation button on populating input box text`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        presenter.onTextChanged(1)

        // then
        verify { view.enableCalculateButton() }
    }

    @Test
    fun `disable calculation button on emptying input box text`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        presenter.onTextChanged(0)

        // then
        verify { view.disableCalculateButton() }
    }

    @Test
    fun `enable clear button on populating input box text`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        presenter.onTextChanged(1)

        // then
        verify { view.enableClearButton() }
    }

    @Test
    fun `disable clear button on emptying input box text`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        presenter.onTextChanged(0)

        // then
        verify { view.disableClearButton() }
    }


    @Test
    fun `set mean on calculate button press`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), TestModel(1.0, 99.0))

        // input ignored; uses TestModel inputs
        presenter.onCalculateButtonPress("1,2")

        // then
        verify { view.setMeanLabel(TestModel.meanPreText + 1.0) }
    }

    @Test
    fun `set invalid mean on calculate button press`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), TestModel(Double.NaN, Double.NaN))

        // input ignored; uses TestModel inputs
        presenter.onCalculateButtonPress("")

        // then
        verify { view.setMeanLabel(TestModel.meanPreText + TestModel.invalid) }
    }

    @Test
    fun `set median on calculate button press`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), TestModel(1.0, 99.0))

        presenter.onCalculateButtonPress("1,2")

        // then
        verify { view.setMedianLabel(TestModel.medianPreText + 99.0) }
    }

    @Test
    fun `set invalid median calculate button press`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), TestModel(Double.NaN, Double.NaN))

        // input ignored; uses TestModel inputs
        presenter.onCalculateButtonPress("")

        // then
        verify { view.setMedianLabel(TestModel.medianPreText + TestModel.invalid) }
    }


    @Test
    fun `clear mean on clear button press`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        presenter.onClearButtonPress()

        // then
        verify { view.setMeanLabel("") }
    }

    @Test
    fun `clear median on clear button press`() {
        // when
        val presenter = Presenter(view, CalculationViewState(), model)

        presenter.onClearButtonPress()

        // then
        verify { view.setMedianLabel("") }
    }
}