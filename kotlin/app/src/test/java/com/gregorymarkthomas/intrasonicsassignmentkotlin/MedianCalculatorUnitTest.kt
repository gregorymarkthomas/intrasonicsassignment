package com.gregorymarkthomas.intrasonicsassignmentkotlin;

import com.gregorymarkthomas.intrasonicsassignmentkotlin.model.MedianCalculator
import org.junit.Assert.*
import org.junit.Test;

/**
 * Tests getMedian() in NumberArrayCalculator.
 *
 * The 'delta' input of assertEquals is required with value of 0 to stop
 * "Ambiguous method call. Both assertEquals(Object, Object) in Assert and assertEquals
 * (double, double) in Assert match" error.
 * This error occurs because getMedian() returns a Double and not double; the compiler does not know
 * which assertEquals function (out of (double, double) and (Object, Object)) I want it to use.
 * Setting the delta tells the compiler I am expecting a legitimate double back. The 0 used for
 * delta says that we are expecting our result double to be exactly what we say it will be, and there
 * will be no variance (say, 0.0001).
 *
 * If the size of the input array for some tests is too large (think 1000000000),
 * then heap space for JVM may be an issue.
 * A value of 1000000000 causes "java.lang.OutOfMemoryError: Java heap space" on Gregory Thomas's
 * Android Studio install (Linux Mint 19).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
class MedianCalculatorUnitTest {
    @Test
    fun empty_array_returns_nan() {
        assertEquals(Double.NaN, MedianCalculator().getMedian(doubleArrayOf()), 0.0);
    }

    @Test
    fun populated_array_returns_Double() {
        assertTrue(MedianCalculator().getMedian(doubleArrayOf(5.0)) is Double);
    }

    @Test
    fun single_input_returns_same() {
        assertEquals(5.0, MedianCalculator().getMedian(doubleArrayOf(5.0)), 0.0);
    }

    @Test
    fun assignment_example() {
        assertEquals(57.5, MedianCalculator().getMedian(doubleArrayOf(23.0, 37.0, 42.0, 55.0, 60.0, 72.0, 83.0, 91.0)), 0.0);
    }

    @Test
    fun single_negative_input_returns_same() {
        assertEquals(-5.0, MedianCalculator().getMedian(doubleArrayOf(-5.0)), 0.0);
    }

    @Test
    fun decimal_numbers_return_decimal_value() {
        assertEquals(2.1, MedianCalculator().getMedian(doubleArrayOf(2.4, 1.8)), 0.0);
    }

    @Test
    fun negative_decimal_numbers_return_decimal_value() {
        assertEquals(-2.1, MedianCalculator().getMedian(doubleArrayOf(-2.4, -1.8)), 0.0);
    }

    /**
     * The rules for calculating median of positive and negative numbers are (should be) the same.
     * We expect the function to order the array as -28, -16, -10, 14, 30.
     */
    @Test
    fun positive_and_negative_whole_numbers() {
        assertEquals(-10.0, MedianCalculator().getMedian(doubleArrayOf(-16.0, -28.0, 14.0, -10.0, 30.0)), 0.0);
    }

    /**
     * Test to check how function fares (in time and memory) with arrLength number of inputs.
     * I am expecting this to be slow at both the for loop in this test (a bit slow),
     * AND within NumberArrayCalculator.getMedian(); it currently sorts the array before finding
     * the median. Arrays.sort() is currently used, which is a mergesort (O(n log n)).
     *
     * TODO: optimise NumberArrayCalculator.getMedian()
     *
     * 'median' variable relies on arrLength being an ODD number.
     */
    @Test
    fun large_input() {
        val arrLength = 99999999
        val arr = DoubleArray(arrLength)

        for(i in arrLength until 0) {
            arr[i] = i.toDouble()
        }

        val median = arr[arrLength/2];
        assertEquals(median, MedianCalculator().getMedian(arr), 0.0);
    }

    @Test
    fun ignores_nans_in_array() {
        assertEquals(57.5, MedianCalculator().getMedian(doubleArrayOf(Double.NaN, 23.0, 37.0, 42.0, 55.0, Double.NaN, 60.0, 72.0, 83.0, Double.NaN, 91.0, Double.NaN)), 0.0);
    }

    @Test
    fun nan_returns_nan() {
        assertEquals(Double.NaN, MedianCalculator().getMedian(doubleArrayOf(Double.NaN)), 0.0);
    }

    @Test
    fun one_digit_one_nan() {
        assertEquals(7.0, MedianCalculator().getMedian(doubleArrayOf(7.0, Double.NaN)), 0.0)
    }
}