package com.gregorymarkthomas.intrasonicsassignmentkotlin;

import com.gregorymarkthomas.intrasonicsassignmentkotlin.model.MeanCalculator
import org.junit.Assert.*
import org.junit.Test

/**
 * Tests getMean() in NumberArrayCalculator.
 *
 * The 'delta' input of assertEquals is required with value of 0 to stop
 * "Ambiguous method call. Both assertEquals(Object, Object) in Assert and assertEquals
 * (double, double) in Assert match" error.
 * This error occurs because getMean() returns a Double and not double; the compiler does not know
 * which assertEquals function (out of (double, double) and (Object, Object)) I want it to use.
 * Setting the delta tells the compiler I am expecting a legitimate double back. The 0 used for
 * delta says that we are expecting our result double to be exactly what we say it will be, and there
 * will be no variance (say, 0.0001).
 *
 * If the size of the input array for some tests is too large (think 1000000000),
 * then heap space for JVM may be an issue.
 * A value of 1000000000 causes "java.lang.OutOfMemoryError: Java heap space" on Gregory Thomas's
 * Android Studio install (Linux Mint 19).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
class MeanCalculatorUnitTest {

    @Test
    fun empty_array_returns_nan() {
        assertEquals(Double.NaN, MeanCalculator().getMean(doubleArrayOf()), 0.0);
    }

    @Test
    fun populated_array_returns_Double() {
        assertTrue(MeanCalculator().getMean(doubleArrayOf(5.0)) is Double);
    }

    @Test
    fun single_input_returns_same() {
        assertEquals(5.0, MeanCalculator().getMean(doubleArrayOf(5.0)), 0.0)
    }

    @Test
    fun single_negative_input_returns_same() {
        assertEquals(-5.0, MeanCalculator().getMean(doubleArrayOf(-5.0)), 0.0)
    }

    /**
     * The assignment says that the mean of this array is 57.9.
     * In this app, the mean value is NOT rounded upwards unless a view's controller code does it
     * itself.
     */
    @Test
    fun assignment_example() {
        assertEquals(57.875, MeanCalculator().getMean(doubleArrayOf(23.0, 37.0, 42.0, 55.0, 60.0, 72.0, 83.0, 91.0)), 0.0)
    }

    @Test
    fun negative_whole_numbers_return_value() {
        assertEquals(-2.0, MeanCalculator().getMean(doubleArrayOf(-2.0, -2.0)), 0.0)
    }

    @Test
    fun decimal_numbers_return_decimal_value() {
        assertEquals(2.0, MeanCalculator().getMean(doubleArrayOf(2.4, 1.6)), 0.0)
    }

    @Test
    fun negative_decimal_numbers_return_decimal_value() {
        assertEquals(-2.0, MeanCalculator().getMean(doubleArrayOf(-2.4, -1.6)), 0.0)
    }

    /**
     * The rules for calculating averages of positive and negative numbers are (should be) the same.
     */
    @Test
    fun positive_and_negative_whole_numbers() {
        assertEquals(-2.0, MeanCalculator().getMean(doubleArrayOf(-16.0, -28.0, 14.0, -10.0, 30.0)), 0.0)
    }

    /**
     * Test to check how function fares (in time and memory) with arrLength number of inputs.
     */
    @Test
    fun large_random_input() {
        val arrLength = 100000000;
        val arr = DoubleArray(arrLength);

        var total = 0.0;
        for(i in 0 until arrLength) {
            arr[i] = Math.random();
            total += arr[i];
        }
        val mean = total / arrLength;

        assertEquals(mean, MeanCalculator().getMean(arr), 0.0)
    }

    @Test
    fun zero_input_returns_zero() {
        assertEquals(0.0, MeanCalculator().getMean(doubleArrayOf(0.0)), 0.0)
    }

    @Test
    fun ignores_nans_in_array() {
        assertEquals(57.875, MeanCalculator().getMean(doubleArrayOf(Double.NaN, 23.0, 37.0, 42.0, 55.0, Double.NaN, 60.0, 72.0, 83.0, Double.NaN, 91.0, Double.NaN)), 0.0)
    }

    @Test
    fun nan_returns_nan() {
        assertEquals(Double.NaN, MeanCalculator().getMean(doubleArrayOf(Double.NaN)), 0.0);
    }
}