package com.gregorymarkthomas.intrasonicsassignmentkotlin

import com.gregorymarkthomas.intrasonicsassignmentkotlin.model.ModelContract

class TestModel(private val meanOutput: Double, private val medianOutput: Double): ModelContract {

    companion object {
        const val meanPreText = "Mean: "
        const val medianPreText = "Median: "
        const val invalid = "INVALID"
    }

    override fun getMeanLabelPreText(): String {
        return meanPreText
    }

    override fun getMedianLabelPreText(): String {
        return medianPreText
    }

    override fun getInvalidLabelText(): String {
        return invalid
    }

    override fun getMean(userInput: String): Double {
        return meanOutput
    }

    override fun getMedian(userInput: String): Double {
        return medianOutput
    }
}