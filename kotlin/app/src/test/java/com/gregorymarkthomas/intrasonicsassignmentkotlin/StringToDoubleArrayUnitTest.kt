package com.gregorymarkthomas.intrasonicsassignmentkotlin;

import com.gregorymarkthomas.intrasonicsassignmentkotlin.model.StringToDoubleArray;
import org.junit.Assert.assertEquals

import org.junit.Test;

/**
 * Tests the regex of the convert() function within StringToDoubleArray class.
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 *
 * Some test results differ from the Java version due to 'limit = 0' in String.split().
 * 'limit = 0' in Java is a special case as it will remove an empty ending element:
 *
 * > "If <i>n</i> is zero then
 * > the pattern will be applied as many times as possible, the array can
 * > have any length, and trailing empty strings will be discarded."
 *
 * Kotlin will no-longer remove empty trailing strings when the (default) value for "limit" is 0,
 * making limit values behave more consistently.
 *
 * https://stackoverflow.com/questions/48697300/difference-between-kotlin-and-java-string-split-with-regex
 */
class StringToDoubleArrayUnitTest {

    @Test
    fun assignment_example() {
        val arr = StringToDoubleArray().convert("23,37,42,55,60,72,83,91")
        assertEquals(8, arr.size)
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    @Test
    fun separate_by_space() {
        val arr = StringToDoubleArray().convert("23 37 42 55 60 72 83 91");
        assertEquals(8, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    @Test
    fun separate_by_space_or_comma() {
        val arr = StringToDoubleArray().convert("23,37 42,55 60,72 83,91");
        assertEquals(8, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    @Test
    fun separate_by_space_or_comma_or_linebreak() {
        val arr = StringToDoubleArray().convert("23, 37 42, 55\n60, 72 83, 91");
        assertEquals(8, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    @Test
    fun separate_by_multiple_spaces() {
        val arr = StringToDoubleArray().convert("23  37   42    55      60       72        83         91         ");
        assertEquals(9, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
        assertEquals(Double.NaN, arr[8], 0.0)
    }

    @Test
    fun separate_by_multiple_commas() {
        val arr = StringToDoubleArray().convert("23,37,,42,,,55,,,,60,,,,,72,,,,,,83,,,,,,,91");
        assertEquals(8, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    @Test
    fun comma_with_space_is_ignored() {
        val arr = StringToDoubleArray().convert("23, 37, 42, 55, , 60, 72, 83, 91");
        assertEquals(8, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    @Test
    fun separate_by_comma_and_space() {
        val arr = StringToDoubleArray().convert("23, 37, 42, 55, 60, 72, 83, 91");
        assertEquals(8, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    @Test
    fun separate_by_line_break() {
        val arr = StringToDoubleArray().convert("23\n37\n42\n55\n60\n72\n83\n91\n");
        assertEquals(9, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
        assertEquals(Double.NaN, arr[8], 0.0)
    }

    @Test
    fun separate_by_comma_and_space_and_linebreak() {
        val arr = StringToDoubleArray().convert("23, \n37, \n42, \n55, \n60, \n72, \n83, \n91");
        assertEquals(8, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    @Test
    fun separate_by_comma_and_linebreak_and_space() {
        val arr = StringToDoubleArray().convert("23,\n 37,\n 42,\n 55,\n 60,\n 72,\n 83,\n 91");
        assertEquals(8, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    @Test
    fun separate_by_space_and_comma_and_linebreak() {
        val arr = StringToDoubleArray().convert("23 ,\n37 ,\n42 ,\n55 ,\n60 ,\n72 ,\n83 ,\n91");
        assertEquals(8, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    @Test
    fun separate_by_space_and_linebreak_and_comma_() {
        val arr = StringToDoubleArray().convert("23 \n,37 \n,42 \n,55 \n,60 \n,72 \n,83 \n,91");
        assertEquals(8, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    @Test
    fun separate_by_linebreak_and_space_and_comma() {
        val arr = StringToDoubleArray().convert("23\n ,37\n ,42\n ,55\n ,60\n ,72\n ,83\n ,91");
        assertEquals(8, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    @Test
    fun separate_by_linebreak_and_comma_and_space() {
        val arr = StringToDoubleArray().convert("23\n, 37\n, 42\n, 55\n, 60\n, 72\n, 83\n, 91");
        assertEquals(8, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    @Test
    fun comma_at_start() {
        val arr = StringToDoubleArray().convert(",23,37,42,55,60,72,83,91");
        assertEquals(9, arr.size);
        assertEquals(Double.NaN, arr[0], 0.0)
        assertEquals(23.0, arr[1], 0.0)
        assertEquals(37.0, arr[2], 0.0)
        assertEquals(42.0, arr[3], 0.0)
        assertEquals(55.0, arr[4], 0.0)
        assertEquals(60.0, arr[5], 0.0)
        assertEquals(72.0, arr[6], 0.0)
        assertEquals(83.0, arr[7], 0.0)
        assertEquals(91.0, arr[8], 0.0)
    }

    @Test
    fun double_comma_at_start() {
        val arr = StringToDoubleArray().convert(",,23,37,42,55,60,72,83,91");
        assertEquals(9, arr.size);
        assertEquals(Double.NaN, arr[0], 0.0)
        assertEquals(23.0, arr[1], 0.0)
        assertEquals(37.0, arr[2], 0.0)
        assertEquals(42.0, arr[3], 0.0)
        assertEquals(55.0, arr[4], 0.0)
        assertEquals(60.0, arr[5], 0.0)
        assertEquals(72.0, arr[6], 0.0)
        assertEquals(83.0, arr[7], 0.0)
        assertEquals(91.0, arr[8], 0.0)
    }

    @Test
    fun whitespace_at_start() {
        val arr = StringToDoubleArray().convert(" 23 37 42 55 60 72 83 91");
        assertEquals(9, arr.size);
        assertEquals(Double.NaN, arr[0], 0.0)
        assertEquals(23.0, arr[1], 0.0)
        assertEquals(37.0, arr[2], 0.0)
        assertEquals(42.0, arr[3], 0.0)
        assertEquals(55.0, arr[4], 0.0)
        assertEquals(60.0, arr[5], 0.0)
        assertEquals(72.0, arr[6], 0.0)
        assertEquals(83.0, arr[7], 0.0)
        assertEquals(91.0, arr[8], 0.0)
    }

    @Test
    fun double_whitespace_at_start() {
        val arr = StringToDoubleArray().convert("  23 37 42 55 60 72 83 91");
        assertEquals(9, arr.size);
        assertEquals(Double.NaN, arr[0], 0.0)
        assertEquals(23.0, arr[1], 0.0)
        assertEquals(37.0, arr[2], 0.0)
        assertEquals(42.0, arr[3], 0.0)
        assertEquals(55.0, arr[4], 0.0)
        assertEquals(60.0, arr[5], 0.0)
        assertEquals(72.0, arr[6], 0.0)
        assertEquals(83.0, arr[7], 0.0)
        assertEquals(91.0, arr[8], 0.0)
    }

    @Test
    fun comma_at_end() {
        val arr = StringToDoubleArray().convert("23,37,42,55,60,72,83,91,");
        assertEquals(9, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
        assertEquals(Double.NaN, arr[8], 0.0)
    }

    @Test
    fun whitespace_at_end() {
        val arr = StringToDoubleArray().convert("23 37 42 55 60 72 83 91 ");
        assertEquals(9, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
        assertEquals(Double.NaN, arr[8], 0.0)
    }

    @Test
    fun comma_at_start_then_minus_value() {
        val arr = StringToDoubleArray().convert(",-23,37,42,55,60,72,83,91");
        assertEquals(9, arr.size);
        assertEquals(Double.NaN, arr[0], 0.0)
        assertEquals(-23.0, arr[1], 0.0)
        assertEquals(37.0, arr[2], 0.0)
        assertEquals(42.0, arr[3], 0.0)
        assertEquals(55.0, arr[4], 0.0)
        assertEquals(60.0, arr[5], 0.0)
        assertEquals(72.0, arr[6], 0.0)
        assertEquals(83.0, arr[7], 0.0)
        assertEquals(91.0, arr[8], 0.0)
    }

    @Test
    fun minus_with_whitespace() {
        val arr = StringToDoubleArray().convert("-23 -37 -42 -55 -60 -72 -83 -91");
        assertEquals(8, arr.size);
        assertEquals(-23.0, arr[0], 0.0)
        assertEquals(-37.0, arr[1], 0.0)
        assertEquals(-42.0, arr[2], 0.0)
        assertEquals(-55.0, arr[3], 0.0)
        assertEquals(-60.0, arr[4], 0.0)
        assertEquals(-72.0, arr[5], 0.0)
        assertEquals(-83.0, arr[6], 0.0)
        assertEquals(-91.0, arr[7], 0.0)
    }

    @Test
    fun minus_with_linebreak() {
        val arr = StringToDoubleArray().convert("-23\n-37\n-42\n-55\n-60\n-72\n-83\n-91");
        assertEquals(8, arr.size);
        assertEquals(-23.0, arr[0], 0.0)
        assertEquals(-37.0, arr[1], 0.0)
        assertEquals(-42.0, arr[2], 0.0)
        assertEquals(-55.0, arr[3], 0.0)
        assertEquals(-60.0, arr[4], 0.0)
        assertEquals(-72.0, arr[5], 0.0)
        assertEquals(-83.0, arr[6], 0.0)
        assertEquals(-91.0, arr[7], 0.0)
    }

    @Test
    fun minus_at_start() {
        val arr = StringToDoubleArray().convert("-,-23,-37,-42,-55,-60,-72,-83,-91");
        assertEquals(9, arr.size);
        assertEquals(Double.NaN, arr[0], 0.0)
        assertEquals(-23.0, arr[1], 0.0)
        assertEquals(-37.0, arr[2], 0.0)
        assertEquals(-42.0, arr[3], 0.0)
        assertEquals(-55.0, arr[4], 0.0)
        assertEquals(-60.0, arr[5], 0.0)
        assertEquals(-72.0, arr[6], 0.0)
        assertEquals(-83.0, arr[7], 0.0)
        assertEquals(-91.0, arr[8], 0.0)
    }

    @Test
    fun minus_and_whitespace_and_comma() {
        val arr = StringToDoubleArray().convert("-23 ,-37 ,-42 ,-55 ,-60 ,-72 ,-83 ,-91");
        assertEquals(8, arr.size);
        assertEquals(-23.0, arr[0], 0.0)
        assertEquals(-37.0, arr[1], 0.0)
        assertEquals(-42.0, arr[2], 0.0)
        assertEquals(-55.0, arr[3], 0.0)
        assertEquals(-60.0, arr[4], 0.0)
        assertEquals(-72.0, arr[5], 0.0)
        assertEquals(-83.0, arr[6], 0.0)
        assertEquals(-91.0, arr[7], 0.0)
    }

    @Test
    fun minus_and_comma_and_whitespace() {
        val arr = StringToDoubleArray().convert("-23, -37, -42, -55, -60, -72, -83, -91");
        assertEquals(8, arr.size);
        assertEquals(-23.0, arr[0], 0.0)
        assertEquals(-37.0, arr[1], 0.0)
        assertEquals(-42.0, arr[2], 0.0)
        assertEquals(-55.0, arr[3], 0.0)
        assertEquals(-60.0, arr[4], 0.0)
        assertEquals(-72.0, arr[5], 0.0)
        assertEquals(-83.0, arr[6], 0.0)
        assertEquals(-91.0, arr[7], 0.0)
    }

    @Test
    fun dots_at_end() {
        val arr = StringToDoubleArray().convert("23,37,42,55,60,72,83,91...........");
        assertEquals(9, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
        assertEquals(Double.NaN, arr[8], 0.0)
    }

    @Test
    fun just_minus() {
        val arr = StringToDoubleArray().convert("-");
        assertEquals(1, arr.size);
        assertEquals(Double.NaN, arr[0], 0.0)
    }

    @Test
    fun just_comma() {
        val arr = StringToDoubleArray().convert(",");
        assertEquals(2, arr.size);
        assertEquals(Double.NaN, arr[0], 0.0)
        assertEquals(Double.NaN, arr[1], 0.0)
    }

    @Test
    fun just_space() {
        val arr = StringToDoubleArray().convert(" ");
        assertEquals(2, arr.size);
        assertEquals(Double.NaN, arr[0], 0.0)
        assertEquals(Double.NaN, arr[1], 0.0)
    }

    @Test
    fun decimal_strings() {
        val arr = StringToDoubleArray().convert("23.0,37.0,42.0,55.0,60.0,72.0,83.0,91.0");
        assertEquals(8, arr.size);
        assertEquals(23.0, arr[0], 0.0)
        assertEquals(37.0, arr[1], 0.0)
        assertEquals(42.0, arr[2], 0.0)
        assertEquals(55.0, arr[3], 0.0)
        assertEquals(60.0, arr[4], 0.0)
        assertEquals(72.0, arr[5], 0.0)
        assertEquals(83.0, arr[6], 0.0)
        assertEquals(91.0, arr[7], 0.0)
    }

    /**
     * This differs from Java version due to String.split() behaviour now being correct.
     */
    @Test
    fun single_digit_and_multiple_spaces() {
        val arr = StringToDoubleArray().convert("7              ")
        assertEquals(2, arr.size)
        assertEquals(7.0, arr[0], 0.0)
        assertEquals(Double.NaN, arr[1], 0.0)
    }
}