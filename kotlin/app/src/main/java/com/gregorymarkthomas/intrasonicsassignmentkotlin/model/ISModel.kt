package com.gregorymarkthomas.intrasonicsassignmentkotlin.model

import android.content.res.Resources
import com.gregorymarkthomas.intrasonicsassignmentkotlin.R

class ISModel(private val resources: Resources): ModelContract {
    override fun getMeanLabelPreText(): String {
        return resources.getString(R.string.mean_equals)
    }

    override fun getMedianLabelPreText(): String {
        return resources.getString(R.string.median_equals)
    }

    override fun getInvalidLabelText(): String {
        return resources.getString(R.string.invalid)
    }

    override fun getMean(userInput: String): Double {
        return MeanCalculator().getMean(StringToDoubleArray().convert(userInput))
    }

    override fun getMedian(userInput: String): Double {
        return MedianCalculator().getMedian(StringToDoubleArray().convert(userInput))
    }

}