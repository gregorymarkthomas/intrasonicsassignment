package com.gregorymarkthomas.intrasonicsassignmentkotlin.presenter

import android.os.Bundle

interface PresenterContract {
    fun onKeyboardEnterButtonPress(inputBoxContents: String)
    fun onTextChanged(inputBoxContentsLength: Int)
    fun onCalculateButtonPress(inputBoxContents: String)
    fun onClearButtonPress()
}