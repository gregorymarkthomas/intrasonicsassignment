package com.gregorymarkthomas.intrasonicsassignmentkotlin.presenter

import com.gregorymarkthomas.intrasonicsassignmentkotlin.model.ModelContract
import com.gregorymarkthomas.intrasonicsassignmentkotlin.view.MainActivityContract
import java.util.*

class Presenter(
    private val view: MainActivityContract,
    private val state: CalculationViewState,
    private val model: ModelContract): PresenterContract {

    init {
        view.setInputBoxText(state.getInputBoxContents())
        toggleViewButtons(state.getInputBoxContents().length)
        view.setMeanLabel(state.getMeanLabel())
        view.setMedianLabel(state.getMedianLabel())

        view.setCalculateButtonListener()
        view.setClearButtonListener()
        view.setInputBoxKeyboardEnterButtonBehaviour()
        view.setInputBoxTextChangedListener()
    }

    override fun onKeyboardEnterButtonPress(inputBoxContents: String) {
        setViewCalculations(inputBoxContents)
    }

    override fun onTextChanged(inputBoxContentsLength: Int) {
        toggleViewButtons(inputBoxContentsLength)
    }

    override fun onCalculateButtonPress(inputBoxContents: String) {
        setViewCalculations(inputBoxContents)
    }

    override fun onClearButtonPress() {
        view.setInputBoxText("")
        view.focusInputBox()
        view.setMeanLabel("")
        view.setMedianLabel("")
    }

    private fun toggleViewButtons(inputBoxContentsLength: Int) {
        val enabled: Boolean = inputBoxContentsLength > 0
        if(enabled) {
            view.enableCalculateButton()
            view.enableClearButton()
        } else {
            view.disableCalculateButton()
            view.disableClearButton()
        }
    }

    private fun setViewCalculations(inputBoxContents: String) {
        view.setMeanLabel(createStatisticalElementString(model.getMeanLabelPreText(), model.getMean(inputBoxContents), model.getInvalidLabelText()))
        view.setMedianLabel(createStatisticalElementString(model.getMedianLabelPreText(), model.getMedian(inputBoxContents), model.getInvalidLabelText()))
    }

    private fun createStatisticalElementString(elementLabelText: String,
                                               elementValue: Double,
                                               invalidLabelText: String): String {
        var output = elementLabelText
        output += if(elementValue.isNaN()) {
            invalidLabelText
        } else {
            String.format(Locale.getDefault(), "%.1f", elementValue);
        }
        return output
    }

}