package com.gregorymarkthomas.intrasonicsassignmentkotlin.model

import java.lang.Double.isNaN
import java.util.*

class MedianCalculator {
    /**
     * Finds the median (the middle) value of an array of numbers.
     *
     * We are finding the "midpoint" element but how we do that depends on if we have an
     * odd or even number of values.
     *
     * What is the midpoint? It is when there is 50% of the elements on the left, and 50% on the right.
     * If there is an ODD number of elements, we can easily pick the midpoint element as there will
     * be an equal number of values either side of it.
     * For EVEN numbers of elements, the midpoint element will not exist in the array;
     * it will probably be a decimal that would be the midpoint between the values of
     * the two "middle" elements.
     * We must use different formula depending on whether there is an odd or even number of input
     * elements.
     * These two formula rely on the array being SORTED.
     *
     * Even: ((arrayLength / 2) + ((arrayLength + 2) / 2)) / 2
     *
     * Odd: (arrayLength + 1) / 2
     *
     * These mathematical formulae return the INDEX of the median that exists in an array of numbers
     * that start from ONE. In the implementation below you will see extra '- 1's being used.
     * The '- 1' (e.g. int upperMidIndex = (int) (medianIndex + 0.5) - 1;) are there because Java
     * starts counting array elements from ZERO and not one; this is an important difference from
     * the mathematical formulae because if we did not minus 1 from the medianIndex, then the
     * incorrect element will be chosen from the array.
     * For the Odd formula, we remove the "+ 1" to achieve the same effect.
     *
     * e.g: 23, 37, 42, 55, 60, 72, 83, 91 (as per assignment sheet)
     * We have 8 elements, so we use the 'even' formula.
     * 8 / 2 = 4
     * (8 + 2) / 2 = 5
     * = (4 + 5) / 2 = 4.5
     * We want the 4.5th element in our array, but of course we only have a 4th and a 5th element.
     * Our 4th element is 55 and our 5th element is 60; what is the midpoint value between those?
     * ((60-55) / 2) + 55 = 57.5
     * 57.5 is our median.
     *
     * Ignores NaN values (by counting them and counteracting them) that appear in the input array.
     * NaN values appear at the END of a sorted array.
     *
     * TODO: 'Arrays.sort(arr)' is doing a mergesort. Remove sorting stage if possible.
     *  Mergesort has a time complexity of O(n log n) (and will never exceed that)
     *
     * @param arr: the array of doubles to find median of.
     * @return median of array {@param arr}, or NaN if {@param arr} is empty or contains just NaN.
     */
    fun getMedian(arr: DoubleArray): Double {
        var median = Double.NaN
        if (arr.isNotEmpty()) {
            Arrays.sort(arr)
            val nanCount = getNaNCount(arr)
            val arrLengthNoNaNs = arr.size - nanCount
            if (arrLengthNoNaNs > 0) {
                if (arrLengthNoNaNs % 2 == 0) {
                    val medianIndex = (((arrLengthNoNaNs / 2.0) + ((arrLengthNoNaNs + 2) / 2.0)) / 2.0)
                    val upperMidIndex = (medianIndex + 0.5) - 1
                    val lowerMidIndex = (medianIndex - 0.5) - 1
                    median = ((arr[upperMidIndex.toInt()] - arr[lowerMidIndex.toInt()]) / 2) + arr[lowerMidIndex.toInt()]
                } else {
                    val medianIndex = arrLengthNoNaNs / 2
                    median = arr[medianIndex]
                }
            }
        }
        return median;
    }

    /**
     * Gets count of '"Not A Number"s' (NaNs) that are found within {@param arr}.
     * @param arr
     * @return count of NaNs
     */
    private fun getNaNCount(arr: DoubleArray): Int {
        var count = 0
        for (num in arr) {
            if (isNaN(num))
                count++;
        }
        return count;
    }
}