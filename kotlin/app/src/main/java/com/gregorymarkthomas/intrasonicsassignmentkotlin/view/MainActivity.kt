package com.gregorymarkthomas.intrasonicsassignmentkotlin.view

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.gregorymarkthomas.intrasonicsassignmentkotlin.R
import com.gregorymarkthomas.intrasonicsassignmentkotlin.model.ISModel
import com.gregorymarkthomas.intrasonicsassignmentkotlin.presenter.CalculationViewState
import com.gregorymarkthomas.intrasonicsassignmentkotlin.presenter.Presenter
import com.gregorymarkthomas.intrasonicsassignmentkotlin.presenter.PresenterContract
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity: AppCompatActivity(), MainActivityContract {

    private val inputBoxStateKey = "input_key_state_key"
    private val meanLabelStateKey = "mean_key_state_key"
    private val medianLabelStateKey = "median_key_state_key"

    private lateinit var presenter: PresenterContract

    override fun onCreate(state: Bundle?) {
        super.onCreate(state)

        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = inflater.inflate(R.layout.main_activity, null)
        setContentView(view)

        val iSState = CalculationViewState(
            state?.getString(inputBoxStateKey),
            state?.getString(meanLabelStateKey),
            state?.getString(medianLabelStateKey)
        )
        presenter = Presenter(this, iSState, ISModel(applicationContext.resources))
    }

    override fun onSaveInstanceState(state: Bundle) {
        state.putString(inputBoxStateKey, getInputBoxText())
        state.putString(meanLabelStateKey, getMeanLabelText())
        state.putString(medianLabelStateKey, getMedianLabelText())
        super.onSaveInstanceState(state)
    }

    override fun setInputBoxText(value: String) {
        input_edit_text.setText(value)
    }

    override fun setInputBoxKeyboardEnterButtonBehaviour() {
        input_edit_text.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                presenter.onKeyboardEnterButtonPress(getInputBoxText())
                true
            } else
                false
        }
    }

    override fun setInputBoxTextChangedListener() {
        input_edit_text.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                // Do nothing
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // Do nothing
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                presenter.onTextChanged(s.length)
            }
        })
    }

    override fun setCalculateButtonListener() {
        calculate_button.setOnClickListener {
            presenter.onCalculateButtonPress(getInputBoxText())
        }
    }

    override fun setClearButtonListener() {
        clear_button.setOnClickListener {
            presenter.onClearButtonPress()
        }
    }

    override fun focusInputBox() {
        input_edit_text.requestFocus()
    }

    override fun setMeanLabel(value: String) {
        mean_text_view.text = value
    }

    override fun setMedianLabel(value: String) {
        median_text_view.text = value
    }

    override fun enableCalculateButton() {
        calculate_button.isEnabled = true
    }

    override fun disableCalculateButton() {
        calculate_button.isEnabled = false
    }

    override fun enableClearButton() {
        clear_button.isEnabled = true
    }

    override fun disableClearButton() {
        clear_button.isEnabled = false
    }

    private fun getInputBoxText(): String {
        return input_edit_text.text.toString()
    }

    private fun getMeanLabelText(): String {
        return mean_text_view.text.toString()
    }

    private fun getMedianLabelText(): String {
        return median_text_view.text.toString()
    }

}