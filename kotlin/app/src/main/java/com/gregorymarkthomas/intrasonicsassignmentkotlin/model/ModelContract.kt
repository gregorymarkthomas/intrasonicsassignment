package com.gregorymarkthomas.intrasonicsassignmentkotlin.model

interface ModelContract {
    fun getMeanLabelPreText(): String
    fun getMedianLabelPreText(): String
    fun getInvalidLabelText(): String
    fun getMean(userInput: String): Double
    fun getMedian(userInput: String): Double
}