package com.gregorymarkthomas.intrasonicsassignmentkotlin.presenter

class CalculationViewState(
    private var inputBoxContents: String? = null,
    private var meanLabelText: String? = null,
    private var medianLabelText: String? = null) {

    fun getInputBoxContents(): String {
        return if (inputBoxContents.isNullOrBlank())
            ""
        else
            inputBoxContents!!
    }

    fun getMeanLabel(): String {
        return if (meanLabelText.isNullOrBlank())
            ""
        else
            meanLabelText!!
    }

    fun getMedianLabel(): String {
        return if (medianLabelText.isNullOrBlank())
            ""
        else
            medianLabelText!!
    }
}