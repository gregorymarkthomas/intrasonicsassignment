package com.gregorymarkthomas.intrasonicsassignmentkotlin.model;

class StringToDoubleArray {
    private val tag = "StringToArrayConverter"
    private val allowedStringSeparators = "[,\\s]{1,}|[,]{1,}|[\\s]{1,}|[.]{2,}"

    fun convert(str: String): DoubleArray {
        val splitStrings = str.split(allowedStringSeparators.toRegex())
        val doubleArr = DoubleArray(splitStrings.size)
        for(i in 0 until splitStrings.size) {
            doubleArr[i] = parseDouble(splitStrings[i]);
        }
        return doubleArr;
    }

    private fun parseDouble(str: String): Double {
        return try {
            str.toDouble()
        } catch(e: NumberFormatException) {
            Double.NaN;
        }
    }
}
