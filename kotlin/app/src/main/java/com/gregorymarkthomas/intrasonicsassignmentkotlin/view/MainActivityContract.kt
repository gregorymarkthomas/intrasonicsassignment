package com.gregorymarkthomas.intrasonicsassignmentkotlin.view

interface MainActivityContract {
    fun setInputBoxText(value: String)
    fun setInputBoxKeyboardEnterButtonBehaviour()
    fun setInputBoxTextChangedListener()
    fun setCalculateButtonListener()
    fun setClearButtonListener()
    fun focusInputBox()
    fun setMeanLabel(value: String)
    fun setMedianLabel(value: String)
    fun enableCalculateButton()
    fun disableCalculateButton()
    fun enableClearButton()
    fun disableClearButton()
}