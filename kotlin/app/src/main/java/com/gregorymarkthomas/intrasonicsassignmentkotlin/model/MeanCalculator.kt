package com.gregorymarkthomas.intrasonicsassignmentkotlin.model

class MeanCalculator {
    /**
     * Finds the mean (average) value of an array of numbers.
     *
     * Adds all the elements up and divides by how many elements there are.
     *
     * Ignores NaN values that appear in the input array.
     *
     * @param arr: the array of doubles to find mean of.
     * @return mean of array {@param arr}, or NaN if {@param arr} is empty or contains just NaN(s).
     */
    fun getMean(arr: DoubleArray): Double {
        var mean: Double = Double.NaN
        if(arr.isNotEmpty()) {
            var total = 0.0
            var nanCount = 0
            for (number in arr) {
                if(number.isNaN())
                    nanCount++
                else
                    total += number
            }
            if(arr.size - nanCount > 0)
                mean = total/(arr.size - nanCount)
        }
        return mean
    }
}