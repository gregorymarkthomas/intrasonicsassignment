# IS assignment

## What is this?

This is a small excercise I did for an interview recently.


## What does the app do?

This app calculates the mean and median of a sequence of numbers entered by the user.


## Why are there two versions of the app?

The [Java version](android/) was done during test time (bar some minor fixes). The [Kotlin version](kotlin/) was done afterwards for practice purposes.


## Where are the APKs?

### Android
[android/app/release/gregory_mark_thomas_is_assignment_java.apk](android/app/release/gregory_mark_thomas_is_assignment_java.apk)

### Kotlin
[kotlin/app/release/gregory_mark_thomas_is_assignment_kotlin.apk](kotlin/app/release/gregory_mark_thomas_is_assignment_kotlin.apk)
